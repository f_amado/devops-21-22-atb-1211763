# Class Assignment 4, Part 1

## 1. Analysis, Design and Implementation

The following assignment was developed using Docker, Docker Desktop, Git for Windows and Gradle.

### \- To create the folder for the first part of the assignment:

Run the following git bash commands:

cd devops-21-22-atb-<student number><br>
mkdir CA4_Part1<br>
cd CA4_Part1<br>
echo "report" >> readme.md<br>
git add readme.md

### \- To create, open and edit the Dockerfile: first case - the chat server is built inside the Dockerfile:

(image available on <https://hub.docker.com/r/frncamado/my_image>)

Run the following Windows CMD commands:

cd devops-21-22-atb-<student number><br>
cd CA4_Part1<br>
echo #dockerfile >> Dockerfile<br>
notepad Dockerfile

Then, write the following commands on the Dockerfile:

FROM ubuntu:xenial

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update -y<br>
RUN apt-get install -y git<br>
RUN apt-get install -y openjdk-8-jdk-headless

WORKDIR /gitrepo

RUN git clone https://f_amado@bitbucket.org/f_amado/devops-21-22-atb-1211763.git

WORKDIR /gitrepo/devops-21-22-atb-1211763/CA2_Part1/gradle_basic_demo

RUN chmod u+x gradlew<br>
RUN ./gradlew clean build

EXPOSE 59001

CMD ./gradlew runServer

After that, save and close (Ctrl+S; Alt+F4) the Dockerfile.

### \- To create, open and edit the Dockerfile: second case - the chat server is built outside the Dockerfile:

(image available on <https://hub.docker.com/r/frncamado/my_image2>)

First, you will have to build the gradle_basic_demo application. On the project, run the following command:

./gradle clean build

Then, run the following Windows CMD command:

copy devops-21-22-atb-1211763\CA2_Part1\gradle_basic_demo\build\libs\basic_demo-0.1.0.jar devops-21-22-atb-1211763\CA4_Part1_alt\basic_demo-0.1.0.jar

After that, run the following Windows CMD commands:

cd devops-21-22-atb-<student number><br>
mkdir CA4_Part1_alt<br>
cd CA4_Part1_alt<br>
echo #dockerfile >> Dockerfile<br>
notepad Dockerfile

Then, write the following commands on the Dockerfile:

FROM ubuntu:bionic

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update -y<br>
RUN apt-get install -y git<br>
RUN apt-get install -y openjdk-11-jdk-headless

WORKDIR /chatserver

COPY basic_demo-0.1.0.jar /chatserver/basic_demo-0.1.0.jar

RUN chmod u+x basic_demo-0.1.0.jar

EXPOSE 59001

CMD java -cp basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001

After that, save and close (Ctrl+S; Alt+F4) the Dockerfile.

### \-To build the image and publish it on Docker Hub:

Make sure you have installed the Docker Desktop application previous to this step. Go to the https://www.docker.com/ website and follow the instructions. You will be prompted to create a Docker account - do it, it is essential for this assignment.

To build the image, run the following Docker command:

docker build -t <image name> .

To publish the image, first create a repository on the Docker Hub website - hub.docker.com. Log in with your Docker account, select the "Create Repository" option, choose the name for it, select the visibility (private or public) and press "Create".

After that, to tag the image, run the following Docker command:

docker tag <image name> <your_repo>/<image name>:<tag name>

After that, open the Docker Desktop application - make sure you've logged into your Docker account. Once there, on the Images tab, press the icon with the three buttons on the image, and select the option "Push to Hub". A repository will soon be created for your image.

### \-To run the server:

To run a container that will expose a port on localhost:59001, run the following Docker command:

docker run -p 59001:59001 -d <image name>

Let the container open with the server running.

### \-To run the client:

On the build.gradle file of the application, change the first argument of the task runClient to '0.0.0.0'. That way, you can establish a connection with the just created container.

After that, run the following command:

./gradlew runClient

The chat pop-up window should show up and allow you to register a username and send messages.

### \- To tag the end of the first part of the assignment:

Execute the following git bash commands:

cd devops-21-22-atb-<student number><br>
git tag ca4-part1<br>
git push origin ca4-part1