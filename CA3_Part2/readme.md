# Class Assignment 3, Part 2

## 1. Analysis, Design and Implementation

The following assignment was developed using Vagrant, Oracle VM VirtualBox (running on a Ubuntu Xenial 64-bit OS), Git for Windows and Gradle.

### \- To create the folder for the second part of the assignment:

Run the following git bash commands:

cd devops-21-22-atb-<student number><br>
mkdir CA3_Part2<br>
touch readme.md<br>
git add CA3_Part2

### \- To commit and push changes:

Execute the following git bash commands:

git commit -a -m "<commit message>"<br>
git push origin master

### \- To clone the vagrant-multi-spring-tut-demo repository and copy the Vagrantfile to the CA3_Part2 directory:

Execute the following git bash commands:

git clone https://<my_address>@bitbucket.org/atb/vagrant-multi-spring-tut-demo.git<br>
cd vagrant-multi-spring-tut-demo<br>
cp Vagrantfile ~/devops-21-22-atb-<student number>/CA3_Part2<br>

### \- To commit and push changes:

Execute the following git bash commands:

git commit -a -m "<commit message>"<br>
git push origin master

### \- To make the necessary edits to the Vagrantfile:

In order to make the repository public, and avoid breaking the provision script on the Vagrantfile, in Bitbucket go to Repository Settings/Repository Details and desselect the option "This is a private repository".

After that, execute the following following Windows CMD commands: 

cd devops-21-22-atb-<student number><br>
cd CA3_Part2<br>
notepad Vagrantfile

On the Vagrantfile, substitute the commands "git clone https://atb@bitbucket.org/atb/tut-basic-gradle.git", "cd tut-basic-gradle" and "sudo cp ./build/libs/basic-0.0.1-SNAPSHOT.war /var/lib/tomcat8/webapps" for the following:
 
git clone https://<my_address>@bitbucket.org/<repository location>/devops-21-22-atb-<student number>.git<br>
cd <location of the React/Spring tutorial application><br>
sudo cp ./build/libs/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT.war /var/lib/tomcat8/webapps

After that, save and close (CTRL+S; ALT+F4) the Vagrantfile.

### \- To adapt the application to generate a .war file during the build process:

After the build process, the React/Spring tutorial application generates a SNAPSHOT.jar file (located in the build/libs directory). In this case, we want to generate a .war file instead, in order to deploy it to the Tomcat server and successfully run the application. For that, some actions are required.

On the build.gradle file, add the following lines, respectively, to the plugins and dependencies tasks:

id 'war'<br>
providedRuntime 'org.springframework.boot:spring-boot-starter-tomcat'

### \- To adapt the application to use Java 8:

On the plugins task, substitute the "id 'org.siouan.frontend-jdk11' version '6.0.0'" line for the following line:

id "org.siouan.frontend-jdk8" version "6.0.0"

Also, substitute the sourceCompatibility = '11' line for the following:

sourceCompatibility = '8'

### \- To adapt the application to initialize the servlet:

In order to initialize the Spring Boot servlet, create a servlet initializer class, with the following content:
```java
package com.greglturnquist.payroll;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

public class ServletInitializer extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(ReactAndSpringDataRestApplication.class);
    }
}
```
### \- To adapt the application to successfully retrieve its resources:

On the componentDidMount method on the app.js file, substitute the path there present for the following:

/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/api/employees

### \- To configure the H2 server:

Edit the application.properties file to have the following:

server.servlet.context-path=/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT<br>
spring.data.rest.base-path=/api<br>
spring.datasource.url=jdbc:h2:tcp://192.168.56.11:9092/./jpadb;DB_CLOSE_DELAY= 1;DB_CLOSE_ON_EXIT=FALSE<br>
spring.datasource.driverClassName=org.h2.Driver<br>
spring.datasource.username=sa<br>
spring.datasource.password=<br>
spring.jpa.database-platform=org.hibernate.dialect.H2Dialect<br>
spring.jpa.hibernate.ddl-auto=update<br>
spring.h2.console.enabled=true<br>
spring.h2.console.path=/h2-console<br>
spring.h2.console.settings.web-allow-others=true

### \- To start running the virtual machine:

Execute the following following Windows CMD command: 

cd devops-21-22-atb-<student number><br>
cd CA3_Part2<br>
vagrant up

### \- To check if the application is working properly:

In a web browser in the local machine, open the following URL, to check the frontend:

<http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/>

To open the H2 console, use the following URL:

<http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/h2-console>

On the console, substitute the connection string (JDBC URL) for the following and press "Connect":

jdbc:h2:tcp://192.168.56.11:9092/./jpadb

You can also open <http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/api> to check the resources currently in your database.

### \- To tag the end of the second part of the assignment:

Execute the following git bash commands:

cd devops-21-22-atb-<student number><br>
git tag ca3-part2<br>
git push origin ca3-part2

## 2. Analysis of an alternative

**Alternative: VMWare Workstation**

VMWare Workstation is, like Oracle VirtualBox, a virtualization platform - a type 2 hypervisor, actually - but there are some big differences between them.
 
Unlike VirtualBox, which is open source software, VMWare Workstation is proprietary software, which means you have to pay a licensing fee to use the software. Still, VMWare Workstation has a (limited in features) free version for personal and educational use.

VMWare Workstation is a hardware-only virtualization platform, whereas VirtualBox can virtualize software too. VMWare Workstation has also a smaller scope of supporting operating systems.

Compared to VirtualBox, WMWare Workstation offers more CPU, RAM and VRAM utilisation to its virtual machines, making it more powerful and more suitable for deploying resource-intensive applications.

VMWare Workstation has also a more steep learning curve than VirtualBox, making it harder for beginners.

Regarding the integration with Vagrant, VirtualBox supports it out of the box, whereas for VMWare Workstation utility and provider plugins must be installed before you can use Vagrant to deploy virtual machines.

## 3. Implementation of the alternative

The following assignment was developed using Vagrant, VMWare Workstation 16 Pro (running on a Ubuntu Xenial 64-bit OS), Git for Windows and Gradle.

### \- To create the folder for the alternative solution for second part of the assignment:

Run the following git bash commands:

cd devops-21-22-atb-<student number><br>
mkdir CA3_Part2_alt<br>

### \- To copy the Vagrantfile to the CA3_Part2_alt directory:

Execute the following git bash commands:

cd vagrant-multi-spring-tut-demo<br>
cp Vagrantfile ~/devops-21-22-atb-<student number>/CA3_Part2_alt<br>
git add Vagrantfile

### \- To commit and push changes:

Execute the following git bash commands:

git commit -a -m "<commit message>"<br>
git push origin master

### \- To make the necessary edits to the Vagrantfile:

Execute the following following Windows CMD commands: 

cd devops-21-22-atb-<student number><br>
cd CA3_Part2_alt<br>
notepad Vagrantfile

On the Vagrantfile, substitute the commands "git clone https://atb@bitbucket.org/atb/tut-basic-gradle.git", "cd tut-basic-gradle" and "sudo cp ./build/libs/basic-0.0.1-SNAPSHOT.war /var/lib/tomcat8/webapps" for the following:
 
git clone https://<my_address>@bitbucket.org/<repository location>/devops-21-22-atb-<student number>.git<br>
cd <location of the React/Spring tutorial application><br>
sudo cp ./build/libs/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT.war /var/lib/tomcat8/webapps

To increase the amount of RAM to the virtual machines, in order to avoid performance issues, add the following script on the beginning of the Vagrantfile:
```ruby
config.vm.provider "vmware_desktop" do |v|
  v.vmx["memsize"] = "2048"
  v.vmx["numvcpus"] = "2"
end
```

Also, in order to avoid collisions with other virtual host networks, change the db machine IP to 192.168.33.11 and the web machine IP to 192.168.33.10.

After that, save and close (CTRL+S; ALT+F4) the Vagrantfile.

### \- To configure the application to run on VMWare Workstation Pro:

To run the application on VMWare's platform, only one small change on the application.properties file is needed.

Substitute the "spring.datasource.url=jdbc:h2:tcp://192.168.56.11:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE" line for the following:

spring.datasource.url=jdbc:h2:tcp://db:9092/./jpadb;DB_CLOSE_DELAY= 1;DB_CLOSE_ON_EXIT=FALSE

Reverse this change and you can run the application on VirtualBox, provided you have the adequate Vagrantfile.

### \- To start running the virtual machine:

First, you'll have to install the Vagrant VMWare Utility plugin, which can be downloaded from <https://www.vagrantup.com/vmware/downloads>.

Then, execute the following Windows CMD command to install the Vagrant VMWare provider plugin: 

vagrant plugin install vagrant-vmware-desktop

Finally, to run the virtual machine execute the following Windows CMD commands:

cd devops-21-22-atb-<student number><br>
cd CA3_Part2_alt<br>
vagrant up --provider vmware_desktop

### \- To check if the application is working properly:

In a web browser in the local machine, open the following URL, to check the frontend:

<http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/>

To open the H2 console, use the following URL:

<http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/h2-console>

On the console, substitute the connection string (JDBC URL) for the following and press "Connect":

jdbc:h2:tcp://db:9092/./jpadb

You can also open <http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/api> to check the resources currently in your database.
