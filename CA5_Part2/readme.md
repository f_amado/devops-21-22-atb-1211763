## 1. Analysis, Design and Implementation

The following assignment was developed using Jenkins, Git for Windows and Gradle.

### \- To create the folder for the second part of the assignment:

Run the following git bash commands:
```bash
cd devops-21-22-atb-<student number>
mkdir CA5_Part2
cd CA5_Part2
echo "report" >> readme.md
git add readme.md
```
### \- To create and edit the Jenkinsfile:

Run the following Windows CMD commands:
```cmd
cd devops-21-22-atb-<student number>
cd CA2_Part2
cd react-and-spring-data-rest-basic
echo #jenkinsfile >> Jenkinsfile
notepad Jenkinsfile
```
Then, write the following on the Jenkinsfile:
```groovy
pipeline {
    agent any
    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git 'https://bitbucket.org/f_amado/devops-21-22-atb-1211763/src/master/CA2_Part2/react-and-spring-data-rest-basic'
            }
        }
        stage('Assemble') {
            steps {
                echo 'Assembling...'
                script {
                    if (isUnix())
                        sh './gradlew clean assemble'
                    else
                        dir('CA2_Part2/react-and-spring-data-rest-basic') {
                            
                            bat 'gradlew clean assemble'
                        }
                }
            }
        }
        stage('Test') {
            steps {
                echo 'Testing...'
                dir('CA2_Part2/react-and-spring-data-rest-basic') {

                    bat 'gradlew check'
                    junit 'build/test-results/test/*.xml'
                }
            }
        }
        stage('Javadoc') {
            steps {
                echo 'Generating Javadoc...'
                dir('CA2_Part2/react-and-spring-data-rest-basic') {
                    
                	bat 'gradlew javadoc'
		        }
            }
        }
        stage('Publish Javadoc') {
            steps {
                echo 'Publishing Javadoc...'
                publishHTML (target: [
                allowMissing: false,
                alwaysLinkToLastBuild: false,
                keepAll: true,
                reportDir: 'CA2_Part2/react-and-spring-data-rest-basic/build/docs/javadoc',
                reportFiles: 'index.html',
                reportName: "Javadoc"
                ])
            }
        }
        stage('Archive') {
            steps {
                echo 'Archiving...'
                archiveArtifacts artifacts: 'CA2_Part2/react-and-spring-data-rest-basic/build/libs/*.war'
            }
        }
        stage('Generate Docker image') {
        steps {
            echo 'Generating Docker image...'
	        dir('CA2_Part2/react-and-spring-data-rest-basic') {
                script {
                    checkout scm
                    def customImage = docker.build("{username}/ca5_pt2:${env.BUILD_ID}")
                    customImage.push()
                    }
                }
            }   
        }
    }
}
```
After that, save and close (Ctrl+S; Alt+F4) the Jenkinsfile. Add the file to your repository using the git add command.

### \- To create and edit the Dockerfile for the last stage of the pipeline:

In order to generate a docker image with Tomcat and the .war file, it is better to create and use a new Dockerfile for that purpose.

To begin, run the following Windows CMD commands:
```cmd
cd devops-21-22-atb-<student number>
cd CA2_Part2/react-and-spring-data-rest-basic
echo #dockerfile >> Dockerfile
notepad Dockerfile
```
Then, write the following commands on the Dockerfile:
```dockerfile
FROM tomcat:8-jdk8-temurin

RUN apt-get update -y
 
RUN apt-get install sudo nano git nodejs npm -f -y

RUN apt-get clean

RUN rm -rf /var/lib/apt/lists/*

RUN mkdir -p /tmp/build

WORKDIR /tmp/build/

RUN git clone https://f_amado@bitbucket.org/f_amado/devops-21-22-atb-1211763.git

WORKDIR /tmp/build/devops-21-22-atb-1211763/CA2_Part2/react-and-spring-data-rest-basic

RUN chmod u+x gradlew

RUN ./gradlew clean build

RUN cp build/libs/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/ 

RUN rm -Rf /tmp/build/

EXPOSE 8080
```
After that, save and close (Ctrl+S; Alt+F4) the Dockerfile. Add the Dockerfile and the build folder to your repository.

### \- To run the pipeline:

Before running the pipeline, it is required to install three plugins - Docker Pipeline, Javadoc and HTML Publisher. In order to do that, from your dashboard go to "System Configuration" and open the Plugin Manager. Search for the plugin you want to install and click "Install now without restart". After that, restart manually Jenkins. Do this for each plugin.

After that, to open Jenkins (if you're using the .war package version), use the following CMD command:
```cmd
java -jar jenkins.war
```
Once Jenkins opens on localhost:8080, login with your credentials. Click on "New Item" on the dashboard and then enter an item name and select "Pipeline". Press "OK" at the end.

On "Advanced Project Options", select "Pipeline script from SCM", with Git as the SCM, insert the repository URL (and the credentials, if private), and add the relative path to the Jenkinsfile (it will be on CA2_Part2/react-and-spring-data-rest-basic). Press "Save".

Back on the pipeline menu, press "Build Now". If the build was successful, you will be able, if you press the build number on the left, to see the artifacts stored (on the main page), the test report (press "Latest Test Result"), and the Javadoc generated, on the menu on the left.

To access the image on Docker Hub, just follow the provided link:

<https://hub.docker.com/r/frncamado/ca5_pt2> 

### \- To tag the end of the second part of the assignment:

Execute the following git bash commands:
```bash
cd devops-21-22-atb-<student number>
git tag ca5-part2
git push origin ca5-part2
```