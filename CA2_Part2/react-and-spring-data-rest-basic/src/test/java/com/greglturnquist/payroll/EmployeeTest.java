package com.greglturnquist.payroll;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class EmployeeTest {

    @Test
    void createValidNewEmployee() {

        //Arrange & Act
        Employee employee = new Employee("Frodo", "Baggins",
                "ring bearer", "Test", 0, "frodo.baggins@me.com");

        //Assert
        assertNotNull(employee);
    }

    @Test
    void createNewEmployeeWithEmptyFirstName() throws IllegalArgumentException{

        //Arrange, Act & Assert
        assertThrows(IllegalArgumentException.class, () -> new Employee("", "Baggins",
                    "ring bearer", "Test", 7, "frodo.baggins@me.com"));
    }

    @Test
    void createNewEmployeeWithNullFirstName() throws IllegalArgumentException{

        //Arrange, Act & Assert
        assertThrows(IllegalArgumentException.class, () -> new Employee(null, "Baggins",
                "ring bearer", "Test", 7, "frodo.baggins@me.com"));
    }

    @Test
    void createNewEmployeeWithEmptyLastName() throws IllegalArgumentException{

        //Arrange, Act & Assert
        assertThrows(IllegalArgumentException.class, () -> new Employee("Frodo", "",
                "ring bearer", "Test", 7, "frodo.baggins@me.com"));
    }

    @Test
    void createNewEmployeeWithNullLastName() throws IllegalArgumentException{

        //Arrange, Act & Assert
        assertThrows(IllegalArgumentException.class, () -> new Employee("Frodo", null,
                "ring bearer", "Test", 7, "frodo.baggins@me.com"));
    }

    @Test
    void createNewEmployeeWithEmptyDescription() throws IllegalArgumentException{

        //Arrange, Act & Assert
        assertThrows(IllegalArgumentException.class, () -> new Employee("Frodo", "Baggins",
                "", "Test", 7, "frodo.baggins@me.com"));
    }

    @Test
    void createNewEmployeeWithNullDescription() throws IllegalArgumentException{

        //Arrange, Act & Assert
        assertThrows(IllegalArgumentException.class, () -> new Employee("Frodo", "Baggins",
                null, "Test", 7, "frodo.baggins@me.com"));
    }

    @Test
    void createNewEmployeeWithEmptyJobTitle() throws IllegalArgumentException{

        //Arrange, Act & Assert
        assertThrows(IllegalArgumentException.class, () -> new Employee("Frodo", "Baggins",
                "ring bearer", "", 7, "frodo.baggins@me.com"));
    }

    @Test
    void createNewEmployeeWithNullJobTitle() throws IllegalArgumentException{

        //Arrange, Act & Assert
        assertThrows(IllegalArgumentException.class, () -> new Employee("Frodo", "Baggins",
                "ring bearer", null, 7, "frodo.baggins@me.com"));
    }

    @Test
    void createNewEmployeeWithNegativeJobYears() throws IllegalArgumentException{

        //Arrange, Act & Assert
        assertThrows(IllegalArgumentException.class, () -> new Employee("Frodo", "Baggins",
                "ring bearer", "Test", -1, "frodo.baggins@me.com"));
    }

    @Test
    void createNewEmployeeWithInvalidEmail() throws IllegalArgumentException{

        //Arrange, Act & Assert
        assertThrows(IllegalArgumentException.class, () -> new Employee("Frodo", "Baggins",
                "", "Test", 7, "frodo+me-com"));
    }

    @Test
    void createNewEmployeeWithEmptyEmail() throws IllegalArgumentException{

        //Arrange, Act & Assert
        assertThrows(IllegalArgumentException.class, () -> new Employee("Frodo", "Baggins",
                "ring bearer", "Test", 7, ""));
    }

    @Test
    void createNewEmployeeWithNullEmail() throws IllegalArgumentException{

        //Arrange, Act & Assert
        assertThrows(IllegalArgumentException.class, () -> new Employee("Frodo", "Baggins",
                "ring bearer", "Test", 7, null));
    }

    @Test
    void getId() {

        //Arrange
        Employee employee = new Employee("Frodo", "Baggins",
                "ring bearer", "Test", 0, "frodo.baggins@me.com");

        //Act
        Long Id = employee.getId();

        //Assert
        assertNull(Id);
    }

    @Test
    void getFirstName() {

        //Arrange
        Employee employee = new Employee("Frodo", "Baggins",
                "ring bearer", "Test", 0, "frodo.baggins@me.com");

        //Act
        String firstName = employee.getFirstName();

        //Assert
        assertEquals("Frodo", firstName);
    }

    @Test
    void getLastName() {

        //Arrange
        Employee employee = new Employee("Frodo", "Baggins",
                "ring bearer", "Test", 0, "frodo.baggins@me.com");

        //Act
        String lastName = employee.getLastName();

        //Assert
        assertEquals("Baggins", lastName);
    }

    @Test
    void getDescription() {

        //Arrange
        Employee employee = new Employee("Frodo", "Baggins",
                "ring bearer", "Test", 0, "frodo.baggins@me.com");

        //Act
        String description = employee.getDescription();

        //Assert
        assertEquals("ring bearer", description);
    }

    @Test
    void getJobTitle() {

        //Arrange
        Employee employee = new Employee("Frodo", "Baggins",
                "ring bearer", "Test", 0, "frodo.baggins@me.com");

        //Act
        String jobTitle = employee.getJobTitle();

        //Assert
        assertEquals("Test", jobTitle);
    }

    @Test
    void getJobYears() {

        //Arrange
        Employee employee = new Employee("Frodo", "Baggins",
                "ring bearer", "Test", 0, "frodo.baggins@me.com");

        //Act
        int jobYears = employee.getJobYears();

        //Assert
        assertEquals(0, jobYears);
    }

    @Test
    void getEmail() {

        //Arrange
        Employee employee = new Employee("Frodo", "Baggins",
                "ring bearer", "Test", 0, "frodo.baggins@me.com");

        //Act
        String email = employee.getEmail();

        //Assert
        assertEquals("frodo.baggins@me.com", email);
    }

}
