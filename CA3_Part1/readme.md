## 1. Analysis, Design and Implementation

The following assignment was developed using Oracle VM VirtualBox (running on a minimal 64-bit Ubuntu OS), Git for Windows, Apache Maven and Gradle.

### \- To create the virtual machine on Oracle VM VirtualBox:

Follow the instructions on the lecture 5 documentation (devops05).

### \- To clone the individual DevOps repository inside the virtual machine:

Execute the following git bash commands:

git clone https://<username>@bitbucket.org/<username>/devops-21-22-atb-<student number>.git

### \- To build and run the Spring Boot tutorial basic project:

Install Apache Maven, executing the following BASH command:

sudo apt install maven

Run the following BASH commands:

cd devops-21-22-atb-<student number>/CA1/tut-react-and-spring-data-rest/basic<br>
chmod a+x ./mvnw<br>
sudo mvn clean install<br>
sudo ./mvnw spring-boot:run

Open the aplication on the host computer using the http://192.168.56.5:8080 URL.

### \- To build and run the gradle_basic_demo project:

Install Gradle, executing the following BASH command:

sudo apt install gradle

Run the following BASH commands:

cd devops-21-22-atb-<student number><br>/CA2_Part1/gradle_basic_demo
chmod a+x ./gradlew<br>
./gradlew clean build<br>
./gradlew runServer

The client can't run on the virtual machine, as the OS has no GUI. For that reason, the client must be run on the host machine. In order to do that, the following line has to be added to the runClient task on the local machine:

args '<virtual machine IP>', '59001'

Then, run the client on the host machine by executing the following Gradle command:

./gradlew runClient

### \- To run, on the virtual machine, the other Gradle tasks written on the Part 1 of CA2:

Run the following BASH commands:

cd devops-21-22-atb-<student number>/CA2_Part1/gradle_basic_demo
./gradlew clean build<br>

Run the following Gradle commands:

./gradlew createBackup

(To verify the execution of this task, run cd backup followed by ls -al, and check if the main and test directories are there)

./gradlew createArchive

(To verify the execution of this task, run cd archive followed by ls -al, and check if the archive.zip file is there)

### \- To run, on the virtual machine, the other Gradle tasks written on the Part 2 of CA2:

Run the following BASH commands:

cd devops-21-22-atb-<student number>/CA2_Part2/react-and-spring-data-rest-basic
chmod a+x ./gradlew<br>

Run the following Gradle commands:

./gradlew copyGeneratedJar

(To verify the execution of this task, run cd dist followed by ls -al, and check if a .jar file is there)

./gradlew deleteWebpackFiles

(To verify the execution of this task, run cd src/resources/static followed by ls -al, and check if the built directory was deleted)

### \- To tag the end of the first part of the assignment:

Create a folder named CA3_Part1, on the devops-21-22-atb-1211763 folder, on the local git repository.

Execute the following git bash commands:

git tag ca3-part1<br>
git push origin ca3-part1
 
