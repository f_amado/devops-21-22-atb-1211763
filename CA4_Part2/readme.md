## 1. Analysis, Design and Implementation

The following assignment was developed using Docker, Docker Desktop, Git for Windows and Gradle.

### \- To create the folder for the second part of the assignment:

Run the following git bash commands:

cd devops-21-22-atb-<student number><br>
mkdir CA4_Part2<br>
cd CA4_Part2<br>
echo "report" >> readme.md<br>
git add readme.md

### \- To create, open and edit the Dockerfile for the web image:

(image available on <https://hub.docker.com/r/frncamado/ca4_part2_web>)

Run the following Windows CMD commands:

cd devops-21-22-atb-<student number><br>
cd CA4_Part2<br>
mkdir web<br>
cd web<br>
echo #dockerfile >> Dockerfile<br>
notepad Dockerfile

Then, write the following commands on the Dockerfile:

FROM tomcat:8-jdk8-temurin

RUN apt-get update -y 

RUN apt-get install sudo nano git nodejs npm -f -y

RUN apt-get clean

RUN rm -rf /var/lib/apt/lists/*

RUN mkdir -p /tmp/build

WORKDIR /tmp/build/

RUN git clone https://f_amado@bitbucket.org/f_amado/devops-21-22-atb-1211763.git

WORKDIR /tmp/build/devops-21-22-atb-1211763/CA2_Part2/react-and-spring-data-rest-basic

RUN chmod u+x gradlew

RUN ./gradlew clean build 

RUN cp build/libs/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/ 

RUN rm -Rf /tmp/build/

EXPOSE 8080

After that, save and close (Ctrl+S; Alt+F4) the Dockerfile. Add the file to your repository using the git add command (git add web).

### \- To create, open and edit the Dockerfile for the database image:

(image available on <https://hub.docker.com/r/frncamado/ca4_part2_db>)

Run the following Windows CMD commands:

cd devops-21-22-atb-<student number><br>
cd CA4_Part2_alt<br>
mkdir db<br>
cd db<br>
echo #dockerfile >> Dockerfile<br>
notepad Dockerfile

Then, write the following commands on the Dockerfile:

FROM ubuntu

RUN apt-get update -y
RUN apt-get install -y openjdk-8-jdk-headless
RUN apt-get install unzip -y
RUN apt-get install wget -y

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app/

RUN wget https://repo1.maven.org/maven2/com/h2database/h2/1.4.200/h2-1.4.200.jar

EXPOSE 8082
EXPOSE 9092

CMD java -cp ./h2-1.4.200.jar org.h2.tools.Server -web -webAllowOthers -tcp -tcpAllowOthers -ifNotExists

After that, save and close (Ctrl+S; Alt+F4) the Dockerfile. Add the file to your repository using the git add command (git add db).

### \- To create, open and edit the docker-compose.yml file:

Run the following Windows CMD commands:

cd devops-21-22-atb-<student number><br>
cd CA4_Part2<br>
echo #docker-compose file >> docker-compose.yml
notepad docker-compose.yml

Then, write the following on the docker-compose file:

```yaml
version: '3'
services:
  web:
    build: web
    ports:
      - "8080:8080"
    networks:
      default:
        ipv4_address: 192.168.33.10
    depends_on:
      - "db"
  db:
    build: db
    ports:
      - "8082:8082"
      - "9092:9092"
    volumes:
      - ./data:/usr/src/data-backup
    networks:
      default:
        ipv4_address: 192.168.33.11
networks:
  default:
    ipam:
      driver: default
      config:
        - subnet: 192.168.33.0/24
```

### \- To build the images and run the containers using Docker Composer:

Run the following Windows CMD commands:

cd devops-21-22-atb-<student number><br>
cd CA4_Part2<br>
docker-compose build<br>

After both images are built successfully, just type the following Docker command:

docker-compose up

### \- To check if the application is working properly:

In a web browser in the local machine, open the following URL, to check the frontend:

http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/

To open the H2 console, use the following URL:

http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/h2-console

On the console, substitute the connection string (JDBC URL) for the following and press "Connect":

jdbc:h2:tcp://192.168.33.11:9092/./jpadb

You can also open http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/api to check the resources currently in your database.

### \- To create a local copy of the database:

First, run the containers in the background with the following Docker command:

docker-compose up --detach

Once the containers are running, use the docker cp command to copy the database file from the container into your local machine:

docker cp ca4_part2_db_1:/usr/src/app/jpadb.mv.db <absolute path to>/data/jpadb.mv.db

Finish by adding the file to your repository using the git add command (git add data).

To check if the copy was sucessful, open the data folder in your local machine or list the contents of the folder using the command line (dir/ls).

### \-To publish the images on Docker Hub:

Make sure you have installed the Docker Desktop application previous to this step. Go to the https://www.docker.com/ website and follow the instructions. You will be prompted to create a Docker account. Follow the provided instrucions.

To publish the images, first create a repository on the Docker Hub website - hub.docker.com. Log in with your Docker account, select the "Create Repository" option, choose the name for it, select the visibility (private or public) and press "Create".

After that, to tag the image, run for each one the following Docker command:

docker tag <image name> <your_repo>/<image name>:<tag name>

After that, open the Docker Desktop application - make sure you've logged into your Docker account. Once there, on the Images tab, press the icon with the three buttons on the image, and select for each one the option "Push to Hub".

### \- To tag the end of the second part of the assignment:

Execute the following git bash commands:

cd devops-21-22-atb-<student number><br>
git tag ca4-part2<br>
git push origin ca4-part2