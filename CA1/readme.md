# Class Assignment 1 Report

## 1. Analysis, Design and Implementation

The following assignment was developed using Git for Windows and Bitbucket.

### \- To copy the directory tut-react-and-spring-data-rest to CA1:

Execute the following git bash commands:

cd devops-21-22-atb-1211763<br>
mkdir CA1<br> 
cp -R ~/devops-21-22-atb-1211763/tut-react-and-spring-data-rest ~/devops-21-22-atb-1211763/CA1<br>  

### \- To commit and push the changes:

Execute the following git bash commands:

git add CA1/<br> 
git commit -a -m "Created CA1 directory and copied tut-react-and-spring-rest directory to CA1"<br> 
git push origin master<br> 

### \- To tag the version as v1.1.0:

Execute the following git bash commands:

git tag v1.1.0<br> 
git push origin v1.1.0<br> 

### \- To develop the new feature (jobYears field on the Employee object):

Add a new attribute to the Employee class;<br> 
Add the attribute to the class' constructor;<br> 
Create getter and setter methods;<br> 
Update equals, hashCode and toString methods;<br> 
Update the employees on the DatabaseLoader class;<br> 
Update the app.js table accordingly;<br> 
Create all relevant unit tests (creation and validation);<br> 
Run the application using the mvnw spring-boot:run command;<br> 
Open http://localhost:8080/ to check the UI;<br> 
Debug using IntellijIDEA and React Development Tools.<br> 

### \- To tag the version as v1.2.0:

Execute the following git bash commands:

git tag v1.2.0<br> 
git push origin v1.2.0<br> 

### \- To tag the end of the first part of the assignment:

Execute the following git bash commands:

git tag ca1-part1<br> 
git push origin ca1-part1<br> 

### \- To create a new branch called "email-field" and work on it:

Execute the following git bash commands:

git branch email-field<br> 
git checkout email-field<br> 

### \- To develop the new feature (email field on the Employee object):

Add a new attribute to the Employee class;<br> 
Add the attribute to the class' constructor;<br> 
Create getter and setter methods;<br> 
Update equals, hashCode and toString methods;<br>
Update the employees on the DatabaseLoader class;<br> 
Update the app.js table accordingly;<br>
Create all relevant unit tests (creation and validation);  
Run the application using the mvnw spring-boot:run command;<br>
Open http://localhost:8080/ to check the UI;<br>
Debug using IntellijIDEA and React Development Tools.<br> 

### \- To merge the email-field branch and push the changes:

Execute the following git bash commands:

git checkout master<br> 
git merge email-field<br> 
git push origin master<br> 

### \- To tag the version as v1.3.0:

Execute the following git bash commands:

git tag v1.3.0<br> 
git push origin v1.3.0<br> 

### \- To create a new branch called "fix-invalid-email" and work on it:

Execute the following git bash commands:

git branch fix-invalid-email<br> 
git checkout fix-invalid-email<br> 

### \- To develop the new feature (new validation for the email field on the Employee object):

Create a RFC5321 regex validation in validEmail method;<br>
Create all relevant unit tests (creation and validation);<br> 
Run the application using the mvnw spring-boot:run command;<br> 
Debug using IntellijIDEA and React Development Tools.<br> 

### \- To merge the fix-invalid-email branch and push the changes:

Execute the following git bash commands:
 
git checkout master<br> 
git merge fix-invalid-email<br> 
git push origin master<br> 

### \- To tag the version as v1.3.1:

Execute the following git bash commands:

git tag v1.3.1<br> 
git push origin v1.3.1<br> 

### \- To tag the end of the assignment:

Execute the following git bash commands:

git tag ca1-part2<br> 
git push origin ca1-part2<br> 

## 2. Analysis of an alternative

Alternative: Mercurial

Mercurial, like Git, is a distributed version control system which supports almost all the main features Git has.

Unlike Git, Mercurial doesn't have a staging area. It suports similar operations, but using a repository clone as a makeshift staging area ("staging clone").

Both Git and Mercurial support branching. Branches in Mercurial refer to a linear line of consecutive changesets, whereas in Git they are references to a certain commit (making them more lightweight and easy to manipulate.

Barring the different syntax, both systems are pretty similar and have almost the same target audience - Git being better suited for very complex projects.  

## 3. Implementation of the alternative

The following alternative assignment was developed using Mercurial for Windows and HelixTeamHub.

### \- To create a remote Mercurial repository:

Create an account on helixteamhub.cloud;<br>
On "Repositories", select "New repository";<br>
Select Mercurial as the version control system and name the repository;<br>
Press "Create".

### \- To create a local Mercurial repository:

Execute the following Windows CMD/PowerShell commands:

hg clone [remote repository address] [destination folder]<br>

### \- To configure your username:

Open the .hg folder in your local repository;
Open the hgrc file with any text editor;<br>
Add the following text to the file:

[ui]<br>
username = [your name] [<your e-mail>]

### \- To copy the directory tut-react-and-spring-data-rest to CA1:

Execute the following Windows CMD/PowerShell commands:

cd [repository address]<br>
mkdir CA1<br> 
move [full path to tut-react-and-spring-data-rest] CA1<br>  

### \- To commit and push the changes:

Execute the following Windows CMD/PowerShell commands:

hg add <br>
hg commit<br> 
hg push [remote repository address]<br> 

### \- To tag the version as v1.1.0:

Execute the following Windows CMD/PowerShell commands:

hg tag v1.1.0<br> 
hg push [remote repository address]<br> 

### \- To develop the new feature (jobYears field on the Employee object):

Add a new attribute to the Employee class;<br> 
Add the attribute to the class' constructor;<br> 
Create getter and setter methods;<br> 
Update equals, hashCode and toString methods;<br> 
Update the employees on the DatabaseLoader class;<br> 
Update the app.js table accordingly;<br> 
Create all relevant unit tests (creation and validation);<br> 
Run the application using the mvnw spring-boot:run command;<br> 
Open http://localhost:8080/ to check the UI;<br> 
Debug using IntellijIDEA and React Development Tools.<br> 

### \- To tag the version as v1.2.0:

Execute the following Windows CMD/PowerShell commands:

hg tag v1.2.0<br> 
hg push [remote repository address]<br> 

### \- To tag the end of the first part of the assignment:

Execute the following Windows CMD/PowerShell commands:

hg tag ca1-part1<br> 
hg push [remote repository address]<br> 

### \- To create a new branch called "email-field" and work on it:

Execute the following Windows CMD/PowerShell commands:

hg branch email-field<br>
hg push --new-branch [remote repository address]<br>

### \- To develop the new feature (email field on the Employee object):

Add a new attribute to the Employee class;<br> 
Add the attribute to the class' constructor;<br> 
Create getter and setter methods;<br> 
Update equals and hashCode;<br>
Update the employees on the DatabaseLoader class;<br> 
Update the app.js table accordingly;<br>
Create all relevant unit tests (creation and validation);  
Run the application using the mvnw spring-boot:run command;<br>
Open http://localhost:8080/ to check the UI;<br>
Debug using IntellijIDEA and React Development Tools.<br> 

### \- To merge the email-field branch and push the changes:

Execute the following Windows CMD/PowerShell commands:

hg update default<br>
hg merge email-field<br>
hg commit
hg push [remote repository address]<br> 

### \- To tag the version as v1.3.0:

Execute the following Windows CMD/PowerShell commands:

hg tag v1.3.0<br> 
hg push [remote repository address]<br> 

### \- To create a new branch called "fix-invalid-email" and work on it:

Execute the following Windows CMD/PowerShell commands:

hg branch fix-invalid-email<br>
hg push --new-branch [remote repository address]<br>

### \- To develop the new feature (new validation for the email field on the Employee object):

Create a RFC5321 regex validation in validEmail method;<br>
Create all relevant unit tests (creation and validation);<br> 
Run the application using the mvnw spring-boot:run command;<br> 
Debug using IntellijIDEA and React Development Tools.<br> 

### \- To merge the fix-invalid-email branch and push the changes:

Execute the following Windows CMD/PowerShell commands:
 
hg update default<br>
hg merge fix-invalid-email<br>
hg commit
hg push [remote repository address]<br> 

### \- To tag the version as v1.3.1:

Execute the following Windows CMD/PowerShell commands:

hg tag v1.3.1<br> 
hg push [remote repository address]<br>

### \- To tag the end of the assignment:

Execute the following Windows CMD/PowerShell commands:

hg tag ca1-part2<br> 
hg push [remote repository address]<br> 