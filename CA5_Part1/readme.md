## 1. Analysis, Design and Implementation

The following assignment was developed using Jenkins, Git for Windows and Gradle.

### \- To create the folder for the first part of the assignment:

Run the following git bash commands:

```bash
cd devops-21-22-atb-<student number>
mkdir CA5_Part1
cd CA5_Part1
echo "report" >> readme.md
git add readme.md
```

### \- To create and edit the Jenkinsfile:

Run the following Windows CMD commands:

```cmd
cd devops-21-22-atb-<student number>
cd CA2_Part1
cd gradle-basic-demo
echo #jenkinsfile >> Jenkinsfile
notepad Jenkinsfile
```

Then, write the following on the Jenkinsfile:

```groovy
pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git 'https://bitbucket.org/f_amado/devops-21-22-atb-1211763'
           }
        }
        stage('Assemble') {
            steps {
                echo 'Assembling...'
                script{
                    if (isUnix())
                        sh './gradlew clean assemble'
                    else
                        dir('CA2_Part1/gradle_basic_demo') {
                            
                            bat 'gradlew clean assemble'
                    }
                }
            }
        }
        stage('Test') {
            steps {
                echo 'Testing...'
                dir('CA2_Part1/gradle_basic_demo') {
                            
                    bat 'gradlew check'
                    junit 'build/test-results/test/*.xml'
                }
            }
        }
        stage('Archive') {
            steps {
                echo 'Archiving...'
                archiveArtifacts artifacts: 'CA2_Part1/gradle_basic_demo/build/distributions/*.tar, CA2_Part1/gradle_basic_demo/build/distributions/*.zip'
            }
        }
    }
}
```

After that, save and close (Ctrl+S; Alt+F4) the Jenkinsfile. Add the file to your repository using the git add command.

### \- To run the pipeline

To open Jenkins (if you're using the .war package version), use the following CMD command:

```cmd
java -jar jenkins.war
```

Once Jenkins opens on localhost:8080, login with your credentials. Click on "New Item" on the dashboard and then enter an item name and select "Pipeline". Press "OK" at the end.

On "Advanced Project Options", select "Pipeline script from SCM", with Git as the SCM, insert the repository URL (and the credentials, if private), and add the relative path to the Jenkinsfile (it will be on CA2_Part1/gradle_basic_demo). Press "Save".

Back on the pipeline menu, press "Build Now". If the build was successful, you will be able, if you press the build number on the left, to see the artifacts stored (on the main page) and the test report (press "Latest Test Results"). 

### \- To tag the end of the first part of the assignment:

Execute the following git bash commands:
```bash
cd devops-21-22-atb-<student number>
git tag ca5-part1
git push origin ca5-part1
```
